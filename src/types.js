const MOVE             = 'karel/MOVE';
const CHANGE_DIRECTION = 'karel/CHANGE_DIRECTION';
const PLACE_MARK       = 'karel/PLACE_MARK';
const REMOVE_MARK      = 'karel/REMOVE_MARK';

export {
    MOVE,
    CHANGE_DIRECTION,
    PLACE_MARK,
    REMOVE_MARK,
};
