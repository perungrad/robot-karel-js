import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

import { Provider, connect } from 'react-redux';
import store from './store';

import * as actions from './actions';
import { NORTH, EAST, SOUTH, WEST, MAP_WIDTH, MAP_HEIGHT } from './config';

const ConnectedApp = connect(
    (state) => {
        return {
            karel: state.karel,
            marks: state.marks,
        };
    }
)(App);

ReactDOM.render(
    <Provider store={store}>
        <ConnectedApp />
    </Provider>,
    document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();

window.krokDopredu = () => {
    const state = store.getState();
    const currentPos = state.karel.position;
    const direction = state.karel.direction;
    const dx = {
        [NORTH]: 0,
        [EAST]: 1,
        [SOUTH]: 0,
        [WEST]: -1,
    };
    const dy = {
        [NORTH]: -1,
        [EAST]: 0,
        [SOUTH]: 1,
        [WEST]: 0,
    };

    const newPosition = {
        x: currentPos.x + dx[direction],
        y: currentPos.y + dy[direction],
    };

    if (
        (newPosition.x >= MAP_WIDTH)
        || (newPosition.x < 0)
        || (newPosition.y >= MAP_HEIGHT)
        || (newPosition.y < 0)
    ) {
        throw new Error('Predo mnou je stena');
    }

    store.dispatch(actions.move(newPosition));
};

window.otocDoprava = () => {
    const state = store.getState();
    const direction = state.karel.direction;
    const nextDirection = {
        [NORTH]: EAST,
        [EAST]: SOUTH,
        [SOUTH]: WEST,
        [WEST]: NORTH,
    };

    store.dispatch(actions.changeDirection(nextDirection[direction]));
};

window.stojisNaZnacke = () => {
    const state = store.getState();
    const position = state.karel.position;

    return state.marks.find(mark => mark.x === position.x && mark.y === position.y) !== undefined;
};

window.jePredTebouStena = () => {
    const state = store.getState();
    const position = state.karel.position;

    if (
        ((state.karel.direction === NORTH) && (position.y === 0))
        || ((state.karel.direction === SOUTH) && (position.y === (MAP_HEIGHT - 1)))
        || ((state.karel.direction === WEST) && (position.x === 0))
        || ((state.karel.direction === EAST) && (position.x === (MAP_WIDTH - 1)))
    ) {
        return true;
    }

    return false;
};

window.polozZnacku = () => {
    const state    = store.getState();
    const position = state.karel.position;
    const isMarked = state.marks.find(mark => mark.x === position.x && mark.y === position.y) !== undefined;

    if (isMarked) {
        throw new Error('Tu už jedna značka je');
    }

    store.dispatch(actions.placeMark(state.karel.position));
};

window.zdvihniZnacku = () => {
    const state    = store.getState();
    const position = state.karel.position;
    const isMarked = state.marks.find(mark => mark.x === position.x && mark.y === position.y) !== undefined;

    if (!isMarked) {
        throw new Error('Nemám čo zdvihnúť. Nie je tu značka.');
    }
    store.dispatch(actions.removeMark(state.karel.position));
};
