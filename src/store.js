import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';

import { NORTH, MAP_HEIGHT } from './config';
import rootReducer from './reducers';

const initialState = {
    karel: {
        position: {
            x: 0,
            y: MAP_HEIGHT - 1,
        },
        direction: NORTH
    },
    marks: [],
};

const enhancers = [];
const middleware = [
    thunk,
];

if (process.env.NODE_ENV === 'development') {
    const devToolsExtension = window.devToolsExtension;

    if (typeof devToolsExtension === 'function') {
        enhancers.push(devToolsExtension());
    }
}

const composedEnhancers = compose(
    applyMiddleware(...middleware),
    ...enhancers
);

const store = createStore(
    rootReducer,
    initialState,
    composedEnhancers
);

export default store;
