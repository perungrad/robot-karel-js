import * as types from './types';

const move = (destination) => {
    return {
        type: types.MOVE,
        payload: {
            destination,
        }
    };
};

const changeDirection = (direction) => {
    return {
        type: types.CHANGE_DIRECTION,
        payload: {
            direction,
        }
    };
};

const placeMark = (position) => {
    return {
        type: types.PLACE_MARK,
        payload: {
            position,
        }
    };
};

const removeMark = (position) => {
    return {
        type: types.REMOVE_MARK,
        payload: {
            position,
        }
    };
};

export {
    move,
    changeDirection,
    placeMark,
    removeMark,
};
