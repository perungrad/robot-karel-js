import React from 'react';
import './App.css';
import { MAP_WIDTH, MAP_HEIGHT, TILE_SIZE, NORTH, SOUTH, WEST, EAST } from './config';

const Karel = ({ karel }) => {
    const tip = {
        x: karel.position.x * TILE_SIZE + TILE_SIZE - Math.round(TILE_SIZE / 10),
        y: karel.position.y * TILE_SIZE + Math.round(TILE_SIZE / 2),
    };

    const centerPoint = {
        x: karel.position.x * TILE_SIZE + Math.round(TILE_SIZE / 2),
        y: karel.position.y * TILE_SIZE + Math.round(TILE_SIZE / 2),
    };

    const baseSize = Math.round(TILE_SIZE * 2 / 3);

    const lengthA = Math.round(baseSize / 2);
    const lengthB = Math.round(baseSize / 5);
    const lengthC = Math.round(baseSize - baseSize / 10);

    const angles  = {}
    angles[NORTH] = 270;
    angles[EAST]  = 0;
    angles[SOUTH] = 90;
    angles[WEST]  = 180;

    return (
        <g transform={`rotate(${angles[karel.direction]} ${centerPoint.x} ${centerPoint.y})`}>
            <path
                d={`
                    M ${tip.x} ${tip.y}
                    L ${tip.x - lengthA} ${tip.y + lengthA}
                    L ${tip.x - lengthA} ${tip.y + lengthB}
                    L ${tip.x - lengthC} ${tip.y + lengthB}
                    L ${tip.x - lengthC} ${tip.y - lengthB}
                    L ${tip.x - lengthA} ${tip.y - lengthB}
                    L ${tip.x - lengthA} ${tip.y - lengthA}
                    Z
                `}
                fill="green"
                stroke="black"
                strokeWidth="1"
            />
        </g>
    );
};

const App = ({ karel, marks }) => {
    const tiles = [];

    for (let x = 0; x < MAP_HEIGHT; x++) {
        for (let y = 0; y < MAP_WIDTH; y++) {
            const isMarked = marks.find(mark => mark.x === x && mark.y === y) !== undefined;

            tiles.push(
                <rect
                    x={x * TILE_SIZE}
                    y={y * TILE_SIZE}
                    width={TILE_SIZE}
                    height={TILE_SIZE}
                    strokeWidth="1"
                    stroke="black"
                    fill={isMarked ? "grey" : "white"}
                    key={`tile-${x}-${y}`}
                />
            );
        }
    }

    return (
        <svg
            xmlns="http://www.w3.org/2000/svg"
            width={MAP_WIDTH * TILE_SIZE}
            height={MAP_HEIGHT * TILE_SIZE}
        >
            {tiles}
            <Karel karel={karel} />
        </svg>
    );
};

export default App;
