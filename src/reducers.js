import { combineReducers } from 'redux';
import * as types from './types';

const karelReducer = (state = {}, action) => {
    switch (action.type) {
        case types.MOVE:
            return {
                ...state,
                position: action.payload.destination,
            };

        case types.CHANGE_DIRECTION:
            return {
                ...state,
                direction: action.payload.direction,
            };

        default:
            return state;
    }
};

const marksReducer = (state = [], action) => {
    switch (action.type) {
        case types.PLACE_MARK:
            return [...state, action.payload.position];

        case types.REMOVE_MARK:
            const position = action.payload.position;
            const idx = state.findIndex(m => m.x === position.x && m.y === position.y);

            if (idx === -1) {
                return state;
            }

            const marks = [...state];

            marks.splice(idx, 1);

            return marks;

        default:
            return state;
    }
};

const reducer = combineReducers({
    karel: karelReducer,
    marks: marksReducer,
});

export default reducer;
