export const NORTH = 0;
export const EAST  = 1;
export const SOUTH = 2;
export const WEST  = 3;

export const MAP_WIDTH  = 20;
export const MAP_HEIGHT = 20;
export const TILE_SIZE  = 20;

